import UIKit
//DATA MODEL
class Weather {
    
    //MARK: Properties
    
    var locationName : String
    
    var lon : Double// location coord
    var lat : Double
    
    var mainWeather : String
    var weatherDescription : String
    var iconString : String
    
    var temperature : Double
    var humidity : Double
    var windSpeed : Double
    
    var code : Int
    
    //MARK: Initialization
    init(locationName: String, lon: Double, lat: Double, description: String,
          mainWeather: String, iconString: String , temp: Double, humidity: Double, windSpeed: Double,
          code: Int) {
        self.locationName = locationName
        self.lon = lon
        self.lat = lat
        
        self.mainWeather = mainWeather
        self.weatherDescription = description
        self.iconString = iconString
        
        self.temperature = temp
        self.humidity = humidity
        self.windSpeed = windSpeed
        
        self.code = code
    }
    
    //MARK: Private Methods
    func printData(){
        print("Location name: ",self.locationName)
        print("Coord - lat: ", self.lat)
        print("Coord - lon: ", self.lon)
        print("Description: ",self.weatherDescription)
        print("Main weather: ",self.mainWeather)
        print("Icon string: ",self.iconString)
        print("Temperature: ",self.temperature)
        print("Humidity: ",self.humidity)
        print("Wind speed: ",self.windSpeed)
        print("Code: ",self.code)
    }
}
